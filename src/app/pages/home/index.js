import React from 'react';
import YouTube from 'react-youtube';
import './style.scss';
import request from 'axios';
import * as R from 'ramda';
import { Button } from 'semantic-ui-react'


const YOUTUBE_API = 'https://www.googleapis.com/youtube/v3'
const key = 'AIzaSyCnLGzL-QLjIj_dDzHCALc7sT-Ae8aVczI'
const channelId = 'UCpXVFuxWtJVF_Jn5_xu9EhQ'
const YoutubeButton =  (
    <div></div>
)
export default class Home extends React.Component{
    constructor(){
        super();
        this.state = {
            videos: [],
            videosRendered:[],
            items: 100,
            loadingState: false,
            iframes: {}
        }
    }

    componentWillMount(){
        request.get(`${YOUTUBE_API}/channels?part=contentDetails&id=${channelId}&key=${key}`)
            .then(res => {
                const { data } = res;
                const item  = data.items[0];
                const { uploads } = item.contentDetails.relatedPlaylists;
                request.get(`https://www.googleapis.com/youtube/v3/playlistItems?part=snippet,contentDetails&maxResults=25&playlistId=${uploads}&key=${key}`)
                       .then(response =>{
                           const videos = response.data.items.map(vid => {
                               console.log(vid)
                              const id = vid.contentDetails.videoId;
                              const title = vid.snippet.title;
                              const thumbnail = `https://i.ytimg.com/vi_webp/${id}/sddefault.webp`
                              return { id,  thumbnail, title }
                            })
                            console.log('cwm vids=>')
                            console.log(videos)
                            const videosRendered = R.take(3, videos)
                            this.setState({ videos, videosRendered })
                       })
            })
    }
    componentDidMount() {
        this.refs.iScroll.addEventListener("scroll", () => {
          if (this.refs.iScroll.scrollTop + this.refs.iScroll.clientHeight >=this.refs.iScroll.scrollHeight){
            this.loadMoreItems();
          }
        });
    }
    loadIframe(id){
        console.log('loading iframe: '+id)
        const { iframes } = this.state;
        const html = (<iframe width="100%" title={id} height="100%" src={`https://www.youtube.com/embed/${id}?autoplay=1`} frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>)
        this.setState({iframes: R.assoc(id, { html }, iframes)}) 
    }
    getIframe(id){
        const iframe = this.state.iframes[id]
        if(!R.isNil(iframe)){
            return iframe.html
        }
        return '';
    }
    isIframeShown(id){
        const iframe = this.state.iframes[id]
        if(!R.isNil(iframe)){
            return false
        }
        return true;
    }
    displayItems() {
        var items = [];
        console.log('displaying items')
        this.state.videosRendered.map( video =>
            {
                
                const { thumbnail, id, title } = video;
                items.push(
                        <div className="video-container" key={id}>
                            <div className='video' onClick={() => this.loadIframe(id)}>
                                {this.isIframeShown(id) && <img src={thumbnail} alt='video'/>}
                                {this.isIframeShown(id) && 
                                    <div className="yb">
                                        <svg height="100%" version="1.1" viewBox="0 0 68 48" width="100%">
                                            <path d="M66.52,7.74c-0.78-2.93-2.49-5.41-5.42-6.19C55.79,.13,34,0,34,0S12.21,.13,6.9,1.55 C3.97,2.33,2.27,4.81,1.48,7.74C0.06,13.05,0,24,0,24s0.06,10.95,1.48,16.26c0.78,2.93,2.49,5.41,5.42,6.19 C12.21,47.87,34,48,34,48s21.79-0.13,27.1-1.55c2.93-0.78,4.64-3.26,5.42-6.19C67.94,34.95,68,24,68,24S67.94,13.05,66.52,7.74z" fill="#212121" fillOpacity="0.8"></path>
                                            <path d="M 45,24 27,14 27,34" fill="#fff"></path>
                                        </svg>
                                    </div>}
                                {this.getIframe(id)}
                            </div>
                            <h3>{title}</h3>
                        </div>
                    );
            }
        )
        return items;
    }
     
    loadMoreItems() {
        this.setState({ loadingState: true });
        const nextVideos = R.slice(this.state.videosRendered.length, this.state.videosRendered.length+3, this.state.videos)
        setTimeout(() => {
            this.setState({ videosRendered: R.union(this.state.videosRendered, nextVideos), loadingState: false });
        }, 100);
    }
    render(){
        return(
            <div  className="row text-center">
                <div className="col-md-12 head">
                    <h1> Mzansi Internet Videos </h1>
                </div>
                <div className="col-md-12">
                    <div className="row">
                        <div className="col-md-12 col-xs-12 videos">
                            <div className="content">
                                <p> Catch up with mzansi's latest treding videos on  social media</p>
                            </div>
                            <div className="content2">
                                <p> Never miss out on all the fun  🙂😂 </p>
                            </div>
                            <div className="content3">
                                <ul>
                                    <li> #Trending🙆🏾‍♂️</li>
                                    <li>  #Mzansi🇿🇦 </li>
                                    <li>  #WhatsHot 🔥</li>
                                </ul>
                            </div>
                            <div ref="iScroll" style={{ height: "90vh", overflow: "auto"}}>
                                
                                {this.displayItems()}
                        
                                {this.state.loadingState ? <p className="loading"> loading More Items..</p> : ""}
                        
                            </div>
                            <div className="content4">
                                <p><i> Endless hours of fun and entertainment</i></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}