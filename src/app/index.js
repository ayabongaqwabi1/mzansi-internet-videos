import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Home from './pages/home'
import { connect } from "react-redux";
import { startup } from './redux/actions/startup';
import 'semantic-ui-css/semantic.min.css'

export class App extends Component {
  render(){
    this.props.startup();
    return (
      <Router>
        <div>
          <Route exact path="/" component={Home} />
        </div>
      </Router>
    );
  }
}
const mapStateToProps = (state) => ({
  state,
});

const mapDispatchToProps = (dispatch) => ({
  dispatch,
});

const mergeProps = (stateProps, dispatchProps) =>
  Object.assign({}, stateProps, dispatchProps, {
    startup: () => startup(dispatchProps.dispatch, stateProps.state),
  });

export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(App)
