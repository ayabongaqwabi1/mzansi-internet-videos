import * as types from './types';
import cookieBakery from 'js-cookie';
import uuid from 'node-uuid';

const newId = () => uuid.v4();

const domain = 'nomava.co.za';

const INTERACTION_COOKIE = 'nomavaInteractionId';
const INTERACTION_COOKIE_LIFETIME_IN_DAYS = 30;


export const setInteractionId = (interactionId) => {
  const type = types.SET_INTERACTION_ID;
  const payload = interactionId;
  return {
    type,
    payload,
  };
};

export const dispatchNewInteractionId = (dispatch, interactionId) => {
  cookieBakery.set(
    INTERACTION_COOKIE,
    interactionId,
    { expires: INTERACTION_COOKIE_LIFETIME_IN_DAYS, domain });
  dispatch(setInteractionId(interactionId));
};


const startUpActions = (dispatch, state) => {
  const cookieInteractionId = cookieBakery.get(INTERACTION_COOKIE);
  const interactionId = cookieInteractionId ||
        state.interactionId ||
        newId();
  dispatchNewInteractionId(dispatch, interactionId);
};

export const startup = (dispatch, state) => {
  startUpActions(dispatch, state)
}

const dispatchResetActions = (dispatch) => {
  dispatchNewInteractionId(dispatch, newId());
};

export const reset = () => {
  const type = types.RESET;
  return {
    type,
    payload: dispatchResetActions,
  };
};
