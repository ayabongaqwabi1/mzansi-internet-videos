import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/rootReducer';

function log({ getState }) {
  return next => action => {
    console.dir(action.type)
    console.dir(action)

    // Call the next dispatch method in the middleware chain.
    const returnValue = next(action)

    console.dir("NEXT STATE")
    console.dir(getState())

    // This will likely be the action itself, unless
    // a middleware further in chain changed it.
    return returnValue
  }
}

export default function configureStore(initialState = new Map()) {
 return createStore(
   rootReducer,
   initialState,
   applyMiddleware(thunk, log)
 );
}