import React from 'react';
import { Route, IndexRoute } from 'react-router';

import Home from '../pages/home'
import page404 from '../pages/page404';

export default function () {
  return (
    <Route path='/' component={Home}>
      <IndexRoute component={Home} />
      <Route path="/*" component={page404} />
    </Route>
  );
}
